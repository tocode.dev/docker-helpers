Be sure to create an ".env" file with your credentials before installing.

Run shell scripts from the terminal, so that you can react to an event if necessary and see the output of the running script.

SonarQube has admin/admin as the default username/password.
