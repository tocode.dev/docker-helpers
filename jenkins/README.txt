
Be sure to create an ".env" file with your credentials before installing.

Run shell scripts from the terminal, so that you can react to an event if necessary and see the output of the running script.

After installation a jenkins_password.txt file will be created, use this password on the following address: http://localhost:<port_from_env_file>
